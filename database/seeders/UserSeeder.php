<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = [
            [
                'name' => 'HR',
                'email' => 'hr@gmail.com',
                'password' => Hash::make('password'),
            ],
            [

                'name' => 'Employee',
                'email' => 'employee@gmail.com',
                'password' => Hash::make('password'),
            ],

        ];

        foreach ($users as $key => $value) {
            $role = Role::create(['name' => $value['name']]);

            $user = User::create($value);

            $user->syncRoles([$role]);
        }
    }
}
