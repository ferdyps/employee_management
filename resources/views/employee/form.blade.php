<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Employee') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="row">
                    <div class="col-12">
                        <div class="card p-4">
                            @if (isset($employee))
                                <form action="{{ url('employee/'.$employee->id) }}" class="default-form" autocomplete="off">
                                    @method('PUT')
                            @else
                                <form action="{{ url('employee') }}" class="default-form" autocomplete="off" method="POST">
                            @endif
                                @csrf
                                <div class="mb-3 form-input">
                                    <label for="role_id" class="form-label">Role</label>
                                    <select class="form-control" name="role_id" id="role_id">
                                      @foreach ($roles as $role)
                                          <option 
                                            @isset($employee)
                                                {{ ($employee->user->roles[0]->id == $role->id) ? 'selected':''  }} 
                                            @endisset value="{{ $role->id }}">{{ $role->name }}</option>
                                      @endforeach
                                    </select>
                                    <div class="invalid-feedback"></div>
                                  </div>
                                <div class="mb-3 form-input">
                                  <label for="exampleInputEmail1" class="form-label">Email address</label>
                                  <input name="email" type="email" class="form-control" id="exampleInputEmail1" value="{{ $employee->user->email ?? null }}">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="name" class="form-label">Name</label>
                                  <input name="nama" type="text" class="form-control" id="name" value="{{ $employee->nama ?? null }}">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="nik" class="form-label">NIK</label>
                                  <input name="nik" type="text" class="form-control" id="nik" value="{{ $employee->nik ?? null }}">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="npwp" class="form-label">NPWP</label>
                                  <input name="npwp" type="text" class="form-control" id="npwp" value="{{ $employee->npwp ?? null }}">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                                  <input name="tempat_lahir" type="text" class="form-control" id="tempat_lahir" value="{{ $employee->tempat_lahir ?? null }}">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="tgl_lahir" class="form-label">Tanggal Lahir</label>
                                  <input name="tgl_lahir" type="date" class="form-control" id="tgl_lahir" value="{{ $employee->tgl_lahir ?? null }}">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                                  <select class="form-control" name="jenis_kelamin" id="jenis_kelamin">
                                    @foreach ($jenis_kelamin as $value)
                                        <option @isset($employee)
                                        {{ ($employee->jenis_kelamin == $value) ? 'selected':''  }} 
                                    @endisset value="{{ $value }}">{{ ucfirst($value) }}</option>
                                    @endforeach
                                  </select>
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="agama" class="form-label">Agama</label>
                                  <select class="form-control" name="agama" id="agama">
                                    @foreach ($agama as $value)
                                        <option 
                                        @isset($employee)
                                            {{ ($employee->agama == $value) ? 'selected':''  }} 
                                        @endisset 
                                        value="{{ $value }}">{{ ucfirst($value) }}</option>
                                    @endforeach
                                  </select>
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="gol_darah" class="form-label">Golongan Darah</label>
                                  <select class="form-control" name="gol_darah" id="gol_darah">
                                    @foreach ($golongan_darah as $value)
                                        <option 
                                        @isset($employee)
                                            {{ ($employee->gol_darah == $value) ? 'selected':''  }} 
                                        @endisset 
                                        value="{{ $value }}">{{ ucfirst($value) }}</option>
                                    @endforeach
                                  </select>
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="no_telp" class="form-label">Nomor telp.</label>
                                  <input name="no_telp" type="text" class="form-control" id="no_telp" value="{{ $employee->no_telp ?? null }}">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="alamat" class="form-label">Alamat</label>
                                  <textarea class="form-control" name="alamat" id="alamat" cols="30" rows="10">{{ $employee->alamat ?? null }}</textarea>
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3 form-input">
                                  <label for="password" class="form-label">Password</label>
                                  <input name="password" type="password" class="form-control" id="password">
                                  <div class="invalid-feedback"></div>
                                </div>
                                <div class="mb-3">
                                  <label for="password_confirmation" class="form-label">Password Confirmation</label>
                                  <input name="password_confirmation" type="password" class="form-control" id="password_confirmation">
                                </div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                              </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('script')
        <script>
            function handleData(){
                location.href = "{{ url('/') }}"
            }
        </script>
    @endsection
</x-app-layout>
