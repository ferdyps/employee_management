<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-4">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="text-gray-900">
                        Hi, {{ auth()->user()->name }}
                    </div>
                    @if (auth()->user()->hasRole('HR'))
                        <a href="{{ url('employee/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Employee</a>
                    @endif
                </div>

                @if (auth()->user()->hasRole('HR'))
                    <div class="row">
                        <div class="col-12">
                            <div class="card p-4 mt-4">
                                <table class="table" id="employee_list">
                                    <thead>
                                    <tr>
                                        <th scope="col">Nama</th>
                                        <th scope="col">Role</th>
                                        <th scope="col">Tempat Lahir</th>
                                        <th scope="col">Tanggal Lahir</th>
                                        <th scope="col">Jenis Kelamin</th>
                                        <th scope="col">Alamat</th>
                                        <th scope="col">No. Telp</th>
                                        <th scope="col">NIK</th>
                                        <th scope="col">NPWP</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
    @section('script')
        <script>
            let employee_list = $('#employee_list').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{{ url("employee/list") }}',
                    type: 'POST',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                },
                columns: [
                    {data: 'nama'},
                    {data: 'role'},
                    {data: 'tempat_lahir'},
                    {data: 'tgl_lahir'},
                    {data: 'jenis_kelamin'},
                    {data: 'alamat'},
                    {data: 'no_telp'},
                    {data: 'nik'},
                    {data: 'npwp'},
                    {data: 'action', orderable: false, searchable: false},
                ]
            });

            function handleData() {
                setTimeout(function () {
                  employee_list.ajax.reload();
                }, 1000);
            }
        </script>
    @endsection
</x-app-layout>
