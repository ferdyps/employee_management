<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory, HasUlids;
    protected $table = 'employee';
    protected $keyType = 'string';

    public $incrementing = false;

    protected $fillable = [
        'nama',
        'nik',
        'npwp',
        'tempat_lahir',
        'tgl_lahir',
        'jenis_kelamin',
        'agama',
        'gol_darah',
        'no_telp',
        'alamat'
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'employee_id', 'id');
    }
}
