<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules = [
            'role_id' => 'required',
            'email' => 'required|email|unique:users,email',
            'nama' => 'required',
            'nik' => 'required|unique:employee,nik',
            'npwp' => 'required|unique:employee,npwp',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required|date',
            'jenis_kelamin' => 'required',
            'agama' => 'required',
            'gol_darah' => 'required',
            'no_telp' => 'required',
            'alamat' => 'required',
            'password' => 'required|confirmed'
        ];

        if (isset($this->employee->id)) {
            $rules['email'] = ['required', 'email', Rule::unique('users')->ignore($this->employee->user->id, 'id')];
            $rules['nik'] = ['required', Rule::unique('employee')->ignore($this->employee->id, 'id')];
            $rules['npwp'] = ['required', Rule::unique('employee')->ignore($this->employee->id, 'id')];
            $rules['password'] = ['nullable', 'confirmed'];
        }

        return $rules;
    }
}
