<?php

namespace App\Http\Controllers;

use App\Http\Requests\EmployeeRequest;
use App\Models\Employee;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class EmployeeController extends Controller
{
    private $jenis_kelamin = [
        'laki-laki',
        'perempuan'
    ];

    private $agama = [
        "islam",
        "protestan",
        "katholik",
        "hindu",
        "budha",
        "konghucu"
    ];

    private $golongan_darah = [
        "O",
        "A",
        "B",
        "AB"
    ];

    public function create()
    {
        $data = [
            'roles' => Role::get(),
            'jenis_kelamin' => $this->jenis_kelamin,
            'agama' => $this->agama,
            'golongan_darah' => $this->golongan_darah
        ];
        return view('employee.form', $data);
    }

    public function store(EmployeeRequest $request)
    {
        try {
            DB::beginTransaction();

            $employee = Employee::create($request->validated());

            $user = User::create([
                'email' => $request->email,
                'name' => $request->nama,
                'password' => Hash::make($request->password),
                'employee_id' => $employee->id
            ]);

            $role = Role::find($request->role_id);

            $user->syncRoles([$role->name]);

            DB::commit();

            return response()->json(['data' => $employee]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function edit(Employee $employee)
    {
        $data = [
            'employee' => $employee->load('user.roles'),
            'roles' => Role::get(),
            'jenis_kelamin' => $this->jenis_kelamin,
            'agama' => $this->agama,
            'golongan_darah' => $this->golongan_darah
        ];
        return view('employee.form', $data);
    }

    public function update(EmployeeRequest $request, Employee $employee)
    {
        try {
            DB::beginTransaction();

            $employee->update($request->validated());

            $user_data = [
                'email' => $request->email,
                'name' => $request->nama,
            ];

            if ($request->has('password')) {
                $user_data['password'] = Hash::make($request->password);
            }

            $employee->user->update($user_data);

            $role = Role::find($request->role_id);

            $employee->user->syncRoles([$role->name]);

            DB::commit();

            return response()->json(['data' => $employee]);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }

    public function list(Request $request)
    {
        $employee = Employee::with('user.roles');
        return datatables()
            ->eloquent($employee)
            ->addColumn('role', function ($r) {
                return $r->user->roles[0]->name;
            })
            ->addColumn('action', function ($r) {
                return '
                    <a href="' . url('employee/' . $r->id . '/edit') . '"><i class="fa fa-edit text-primary"></i></a>
                    <button class="delete-data" data-url="' . url('employee') . '" data-id="' . $r->id . '"><i class="fa fa-trash text-danger"></i></button>
                ';
            })
            ->rawColumns(['action'])
            ->toJson();
    }

    public function destroy(Employee $employee)
    {
        try {
            DB::beginTransaction();

            User::where('employee_id', $employee->id)->delete();
            $employee->delete();

            DB::commit();

            return response()->json(['message' => 'Success']);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 500);
        }
    }
}
